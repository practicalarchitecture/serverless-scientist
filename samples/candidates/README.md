# Experiments

## Experiment: rounding numbers

This experiment provides multiple candidates for rounding numbers.

### Candidate 1: python-3-round/

Python 3.7 implementation using Python's native `round()`.

TODO: Describe deviations in candidates. Rounding 12.5 yields 12.

### Candidate 2: python-3-math/

Python 3.7 implementation using `math.floor()` and `math.ceil()`.

TODO: Describe deviations in candidates.

## Experiment: sorting lists of strings

This experiment provides multiple candidates for sorting a
comma-separated list of strings.

TODO Implement parsing incoming list as normal list in JSON,
not as list in comma-separated string.

### Candidate 1: python-3-sorted/

Python 3.7 implementation using Python's native `sorted(list)`.

TODO: Describe deviation(s) in this function.

### Candidate 2: python-3-mergesort/

Python 3.7 implementation using a local implementation of
merge sort.

TODO: Describe deviation(s) in this function.

### Candidate 3: go1-sort/

Go 1.x implementation using Go's standard `sort.Strings()`.

TODO: Describe deviation(s) in this function.

### Candidate 4: ruby-2-sort/

Ruby 2.5 implementation using Ruby's standard `Array#sort()`.

TODO: Describe deviation(s) in this function.

## Experiment: generating QR codes

This experiment provides multiple candidates for generating QR codes.

### Candidate 1: node8-qr-image/

Node 8 implementation using `qr-image` library.

TODO: Describe deviations in candidates.

## Candidate 2: TODO

Python 3.7 implementation using ...

TODO: Describe deviations in candidates.
