const LanguageDetect = require('languagedetect');
const lngDetector = new LanguageDetect('iso2');

exports.lambdaHandler = function (event, context, callback) {
  console.log("[INFO] lambdaHandler() started, event: " + JSON.stringify(event))

  // Prepare default response that'll be used when request is incorrect.
  response = {
    'statusCode': 404,
    'headers': { 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' },
    'body': JSON.stringify({ "error": "Missing text argument." })  // This is different than in control on purpose.
  }

  // Check required query string parameter `text`.
  if (!event.queryStringParameters ||
    !event.queryStringParameters.text) {
    callback(null, response)
    return
  }

  // Get text of which the language needs to be detected.
  text = event.queryStringParameters.text.substring(0, 1000);

  try {
    // Detect language, take best scoring language.
    languages = lngDetector.detect(text)
    best_scoring = languages[0]
    language = best_scoring[0]
  } catch (error) {
    language = ""
  }

  response = {
    'statusCode': 200,
    'headers': { 'Content-Type': 'text/plain', 'Access-Control-Allow-Origin': '*' },
    'body': language
  }

  console.log("[INFO] response: " + JSON.stringify(response))

  callback(null, response)
};