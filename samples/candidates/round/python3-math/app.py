import json
import math
import sys
import logging

# Set up logger.
logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.INFO)

def lambda_handler(event, context):
    LOGGER.info("lambda_handler() started, event: %s", event)

    response = {}

    # TODO Instead of testing for things, and optionally creating the
    # error response: create the error response as default, and only
    # complete/change the response if it meets the conditions.

    if event['queryStringParameters'] \
       and 'number' in event['queryStringParameters'] \
       and float(event['queryStringParameters']['number']):
        number = float(event['queryStringParameters']['number'])
        rounded_number = do_round(number)

        response = {"statusCode": 200,
                    "headers": {'Content-Type': 'application/json'},        
                    "body": json.dumps({
                        "number": number,
                        "rounded_number": rounded_number
                    })}
    else:
        response = {"statusCode": 404,
                    "headers": {'Content-Type': 'application/json'},        
                    # This error responds deviates from control on purpose.
                    "body": json.dumps({"error": "Pass a (floating-point) number as parameter, e.g., `?number=4.2`."})}

    LOGGER.info("response: %s", response)
    return response

def do_round(number):
    LOGGER.info("do_round() started, number: %f", number)

    x = math.floor(number)
    rounded_number = x if (number - x) < 0.50  else math.ceil(number)
    return rounded_number
