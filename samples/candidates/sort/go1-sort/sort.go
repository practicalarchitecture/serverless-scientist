package main

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"
	"sort"
	"strings"

	b64 "encoding/base64"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

// Request is the structure we expect the request data to be in.
type Request struct {
	List string `json:"list"`
}

// EmptyResponse is the response structure for once no (valid) data was supplied in request.
type EmptyResponse struct {
	// TODO Currently, this differs from Node.js and Python response bodies.
}

// SortedResponse is for a valid response, returning both the original and sorted list.
type SortedResponse struct {
	List       string `json:"list"`
	SortedList string `json:"sorted_list"`
}

func sortList(list string) string {
	// TODO Instead of splitting a string, use an array of values.
	slice := strings.Split(list, ",")
	sort.Strings(slice)
	return strings.Join(slice, ",")
}

// lambdaHandler() returns JSON object with original list and sorted list.
func lambdaHandler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	// HERE
	var buf bytes.Buffer
	var logger = log.New(&buf, "[INFO]: ", log.Lshortfile)
	logger.Println("lambdaHandler() started, event:", request)

	// Declare default response as empty JSON body.
	emptyResponse := new(EmptyResponse)
	jsonResponse, _ := json.Marshal(emptyResponse)

	var body string
	var req Request

	// Because of setting of binary media types in API Gateway, AWS Lambda might
	// receive incoming JSON strings encoded as base-64. If so, then decode it first.
	if request.IsBase64Encoded {
		bodyDecoded, _ := b64.StdEncoding.DecodeString(request.Body)
		body = string(bodyDecoded)
	} else {
		body = request.Body
	}

	// Try to unmarshall request body to Request structure.
	// This will automagically check existence of "list" key as well.
	if err := json.Unmarshal([]byte(body), &req); err != nil {
		// If that doesn't work, return the (empty) response.
		// TODO Add proper `application/json` as Content-Type header.
		// TODO Return proper error message about wrong / invalid JSON.
		return events.APIGatewayProxyResponse{Body: string(jsonResponse), StatusCode: http.StatusOK}, nil
	}

	// Sort the list.
	list := req.List
	sortedList := sortList(list)

	// TODO Unsorted and sorted string might contain spaces. Fix that.

	// Create the response object with list and sortedList, and marshall it.
	sortedResponse := SortedResponse{list, sortedList}
	res, _ := json.Marshal(sortedResponse)

	// HERE

	return events.APIGatewayProxyResponse{Body: string(res), StatusCode: http.StatusOK}, nil
}

func main() {
	lambda.Start(lambdaHandler)
}
