import json
import random
import sys
import base64
import logging

# Set up logger.
logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.INFO)


def lambda_handler(event, context):
    LOGGER.info("lambda_handler() started, event: %s", event)

    response = {}

    request_body = ""

    # Because of binary asset setting in API Gateway, body comes encoded in base 64.
    # So, if that's the case, decode the body first.
    if 'isBase64Encoded' in event and event['isBase64Encoded']:
      request_body = base64.b64decode(event['body'])
    else:
      request_body = event['body']

    body = json.loads(request_body)

    if 'list' in body:
        list = body['list']

        # TODO Instead of splitting a string, use an array of values.
        sorted_list = ",".join(str(i) for i in do_mergesort(list.split(',')))

        # TODO Unsorted and sorted string contain spaces. Fix that.

        response = { "statusCode": 200,
                     "headers": {'Content-Type': 'application/json'},
                     "body": json.dumps({
                         "list": list,
                         "sorted_list": sorted_list }) }
    else:
        response = { "statusCode": 200,
                     "headers": {'Content-Type': 'application/json'},
                     "body": json.dumps({ }) }
    
    LOGGER.info("response: %s", response)
    return response


def do_mergesort(x):
    # Since this function is called recursively, we're not logging this.

    if len(x) < 2:return x

    result,mid = [],int(len(x)/2)

    y = do_mergesort(x[:mid])
    z = do_mergesort(x[mid:])

    while (len(y) > 0) and (len(z) > 0):
            if y[0] > z[0]:result.append(z.pop(0))   
            else:result.append(y.pop(0))

    result.extend(y+z)
    return result
