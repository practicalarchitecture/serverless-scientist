exports.lambdaHandler = function (event, context, callback) {
  console.log("[INFO] lambdaHandler() started, event: ", event)

  response = {}

  // TODO Instead of testing for things, and optionally creating the
  // error response: create the error response as default, and only
  // complete/change the response if it meets the conditions.

  if (event.queryStringParameters &&
      event.queryStringParameters.number &&
      parseFloat(event.queryStringParameters.number)) {  
    randomNumber = parseFloat(event.queryStringParameters.number)
    roundedNumber = do_round(randomNumber)

    response = {
        'statusCode': 200,
        'headers': {'Content-Type': 'application/json'},        
        'body': JSON.stringify({
                number: randomNumber,
                rounded_number: roundedNumber
            })
    }
  } else {
    response = {
        'statusCode': 404,
        'headers': {'Content-Type': 'application/json'},        
        'body': JSON.stringify({"error": "Pass a number as query string parameter, e.g., `?number=4.2`."})
    }
  }
  
  console.log("[INFO] response: ", response)
  callback(null, response)
};

function do_round(number) {
  console.log("[INFO] do_round() started, number: ", number)

  return Math.round(number);
}