import json
import logging

from metrics_publisher import MetricsPubisher

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.DEBUG)

BODY_EQUALITY = "body_equality"


def compare_body_json(json_str1, json_str2):

    if json_str1 and json_str2:
        json1 = json.loads(json_str1)
        json2 = json.loads(json_str2)
        return json1 == json2

    return json_str1 == json_str2


def extract_body_element(payload):

    if not "body" in payload["received_response"]:
        return None

    return payload["received_response"]["body"]


def compare(control, candidate, arguments_list=None):

    LOGGER.debug("body.compare() arguments_list %s:", arguments_list)
    if candidate["run_id"] != control["run_id"]:
        LOGGER.error(
            "candidate['run_id'] != control['run_id'] %s and %s",
            candidate["run_id"],
            control["run_id"],
        )

    control_content_type = None
    if (
        "headers" in control["received_response"]
        and "content-type" in control["received_response"]["headers"]
    ):
        control_content_type = control["received_response"]["headers"]["content-type"]

    control_body = extract_body_element(control)
    candidate_body = extract_body_element(candidate)

    if control_content_type == "application/json":
        result = compare_body_json(control_body, candidate_body)
    else:
        result = control_body == candidate_body

    metrics_pubsliher = MetricsPubisher()

    if result:
        metric_name = metrics_pubsliher.success_name(BODY_EQUALITY)
    else:
        metric_name = metrics_pubsliher.failure_name(BODY_EQUALITY)
        LOGGER.info(
            # TODO Is original request available? Then we should log that as well,
            # as it gives context for the response body comparison.
            "Body comparison failed: control_body: %s candidate_body: %s",
            control_body,
            candidate_body,
        )

    metrics_pubsliher.publish_fact(
        metric_name,
        control["experiment_name"],
        candidate["run_type"],
        candidate["implementation_name"],
    )

    return result
