import logging

from metrics_publisher import MetricsPubisher

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.DEBUG)

HEADER_EQUALITY = "header_equality"


def extract_headers(payload):

    if not "headers" in payload["received_response"]:
        return {}

    return payload["received_response"]["headers"]


def compare(control, candidate, arguments_list):
    LOGGER.debug("headers.compare() arguments_list %s:", arguments_list)

    control_headers = extract_headers(control)
    candidate_headers = extract_headers(candidate)
    LOGGER.debug("control_headers %s:", control_headers)
    LOGGER.debug("candidate_headers %s:", candidate_headers)

    headers_same = True
    for header in arguments_list:
        LOGGER.debug("header: %s", header)
        # check if header is present in both or not present in both
        headers_same = (header in control_headers) == (header in candidate_headers)
        if headers_same and header in control_headers:
            headers_same = control_headers[header] == candidate_headers[header]
        if not headers_same:
            break

    metrics_publisher = MetricsPubisher()

    if headers_same:
        metric_name = metrics_publisher.success_name(HEADER_EQUALITY)
    else:
        metric_name = metrics_publisher.failure_name(HEADER_EQUALITY)
        LOGGER.info(
            """Headers comparison failed: control_headers: %s candidate_headers: %s""",
            control_headers,
            candidate_headers,
        )

    metrics_publisher.publish_fact(
        metric_name,
        control["experiment_name"],
        candidate["run_type"],
        candidate["implementation_name"],
    )

    return headers_same

