import logging
import os

from datetime import datetime
from elasticsearch import Elasticsearch

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.DEBUG)

METRICS_HOST = os.environ.get("METRICS_HOST", None)
METRICS_INDEX = "metrics"
FAILURE_INDEX = "failures"

es = None
try:
    LOGGER.debug("METRICS_HOST: %s", METRICS_HOST)
    if METRICS_HOST:
        es = Elasticsearch([METRICS_HOST])
    else:
        LOGGER.warning("METRICS_HOST env variable not set")
except Exception as ex:
    LOGGER.exception(
        "Failed to initialize Elasticsearch on host %s port 9200", METRICS_HOST
    )


class MetricsPubisher:
    def success_name(self, name):
        return f"{name}_success"

    def failure_name(self, name):
        return f"{name}_fail"

    def es_index(self, index, doc_type, body):
        if es:
            es.index(index=index, doc_type=doc_type, body=body)
        else:
            LOGGER.warning(
                "Not publishing metrics because ElasticSearch client is not initialized"
            )

    def publish_fact(self, metric_name, experiment_name, run_type, name):
        try:
            details = {
                "@timestamp": datetime.now(),
                "metric_name": metric_name,
                "experiment_name": experiment_name,
                "run_type": run_type,
                "implementation_name": name.replace(" ", "_"),
            }

            self.es_index(METRICS_INDEX, "metric", details)

        except:
            LOGGER.exception(
                "Failed to post metric %s with details %s to %s",
                metric_name,
                details,
                METRICS_HOST,
            )

    def publish_value(self, metric_name, experiment_name, run_type, name, value):
        try:
            details = {
                "@timestamp": datetime.now(),
                "metric_name": metric_name,
                "experiment_name": experiment_name,
                "run_type": run_type,
                "implementation_name": name.replace(" ", "_"),
                "value": value,
            }

            self.es_index(METRICS_INDEX, "metric", details)

        except:
            LOGGER.exception(
                "Failed to post metric %s with details %s to %s",
                metric_name,
                details,
                METRICS_HOST,
            )

    def publish_failure(self, metric_name, experiment_name, run_type, name, run_id):
        try:
            details = {
                "@timestamp": datetime.now(),
                "metric_name": metric_name,
                "experiment_name": experiment_name,
                "run_type": run_type,
                "implementation_name": name.replace(" ", "_"),
                "run_id": run_id,
            }

            self.es_index(FAILURE_INDEX, "failure", details)

        except:
            LOGGER.exception(
                "Failed to post metric %s with details %s to %s",
                metric_name,
                details,
                METRICS_HOST,
            )
