import json
import logging
import os

import boto3

from dynamodb_json import json_util as dyndb_json_util

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.DEBUG)

RESULT_COMPARATOR_ARN = os.environ.get("RESULT_COMPARATOR_ARN")


class RunResultsStore:

    dynamodb_client = boto3.resource("dynamodb")
    results_table = dynamodb_client.Table(os.environ["RESULTS_TABLE"])

    def store_result(self, run_result):
        LOGGER.info("run_result: %s", run_result)

        rec_id = "{}_{}".format(run_result["run_id"], run_result["arn"])
        LOGGER.debug("Storing result with id: %s", rec_id)
        self.results_table.put_item(
            Item={
                "id": rec_id,
                "experiment_name": run_result["experiment_name"],
                "run_id": run_result["run_id"],
                "run_type": run_result["run_type"],
                "implementation_name": run_result["implementation_name"],
                "arn": run_result["arn"],
                "received_response": run_result["received_response"],
                "run_metrics": run_result["metrics"],
                "comparators": run_result["comparators"],
                "request_payload": run_result["request_payload"],
            }
        )

    def get_run_results(self, run_id):

        query_result = self.results_table.query(
            IndexName="run_id_gsi",
            KeyConditionExpression="#run_id = :run_id",
            ExpressionAttributeNames={"#run_id": "run_id"},
            ExpressionAttributeValues={":run_id": run_id},
        )
        run_results = dyndb_json_util.loads(query_result)
        return run_results["Items"]
