import json
import logging

# Set up logger.
logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.INFO)


def lambda_handler(event, context):
    LOGGER.info("lambda_handler() started, event data: %s", event)

    query_string_parameters = event["queryStringParameters"]
    LOGGER.info("request query string parameters: %s", query_string_parameters)
    
    response = { "statusCode": 404,
                 "headers": {"Content-Type": "text/plain"},
                 "body": "Pass a number as query string parameter, e.g., `?number=2.4`.\n" }

    if ( query_string_parameters and
        "number" in query_string_parameters and
        float(query_string_parameters["number"])):

        number = float(query_string_parameters["number"])
        rounded_number = do_round(number)

        return { "statusCode": 200,
                 "headers": {"Content-Type": "text/plain"},
                 "body": str(rounded_number) }

    LOGGER.info("lambda_handler() response: %s", response)
    return response


def do_round(number):
    # Peculiarity in Python 3: round(20.5) => 20, round(21.5) => 22, round(22.5) => 22.
    # Explanation: Python 3 implements rounding half to even, https://en.wikipedia.org/wiki/Rounding#Round_half_to_even.
    # while Python 2 implements rounding half away from zero, https://en.wikipedia.org/wiki/Rounding#Round_half_away_from_zero.
    return round(number)