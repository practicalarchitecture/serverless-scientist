require 'json'
require 'logger'

LOGGER = Logger.new(STDOUT)
LOGGER.level = Logger::INFO


def lambda_handler(event:, context:)
  LOGGER.info("lambda_handler() started, event data: %s" % event)

  request_body = event["body"]
  LOGGER.info("request body: %s" % request_body)

  response = { "statusCode": 400, # Bad Request
               "headers": {"Content-Type": "application/json"},
               "body": JSON.dump({"message": "400 Bad Request"}) }

  begin
    if request_body
      body = JSON.parse(request_body)
      sorted_body = do_sort(body)

      # TODO Change the status code from 501 (Not Implemented) to 200 (OK).
      response = { "statusCode": 501,
                  "headers": {"Content-Type": "application/json"},
                 # TODO Change the response body to JSON.dump(sorted_body).
                  "body": JSON.dump({"message": "Almost implemented"}) }
    end
  rescue
    response = { "statusCode": 400, # Bad Request
                "headers": {"Content-Type": "application/json"},
                "body": JSON.dump({"message": "Can't sort that JSON object"}) }
  end

  LOGGER.info("handle_post() response: %s" % response)
  response
end

def do_sort(list)
  list.sort
end
