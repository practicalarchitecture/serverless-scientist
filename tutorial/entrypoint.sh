#!/bin/bash

nohup code-server --no-auth --allow-http --port 8080 /workspace &

/opt/instruqt/docker-entrypoint.sh
