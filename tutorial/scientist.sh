#!/bin/bash

info() {
  CWD=`pwd`

  echo "Below are some relevant settings thay you may need during the tutorial."

  # Determine the Serverless Scientist URL
  URL_LINE=`aws cloudformation describe-stacks --stack-name serverless-scientist-v1 --query "Stacks[0].Outputs[?OutputKey=='ServiceEndpoint'].OutputValue" --output text`
  if [ $? -eq 0 ]; then
      URL=${URL_LINE/ANY - /}
      echo "Serverless Scientist URL: $URL/scientist/{experiment}"
  else
      echo "Looks as if Serverless Scientist is not deployed yet. "
  fi

  echo
  cd $CWD
}

dashboard() {
  GRAFANA_PASSWORD=`cat /tmp/grafana_password.txt`
  GRAFANA_HOST=`aws cloudformation describe-stacks --stack-name serverless-scientist-v1 --query "Stacks[0].Outputs[?OutputKey=='GrafanaHost'].OutputValue" --output text`
  if [ $? -eq 0 ]; then
      echo "Grafana URL: http://${GRAFANA_HOST}:3000/d/experiments/experiments"
      echo "Grafana username: admin"
      echo "Grafana password: ${GRAFANA_PASSWORD}"
  else
      echo "Grafana not available yet"
  fi
}

update-experiments() {
  aws s3 cp /workspace/serverless-scientist/tutorial/tutorial_experiments.yaml s3://scientist-experiments-${AWS_ACCOUNT_USERNAME}/

  if [ $? -eq 0 ]; then
      echo
      echo "Experiments file uploaded successfully"
  else
      echo
      echo "Failed to upload experiments file."
  fi
}


main() {
  case "${1}" in
    info)
      info
      ;;
    update-experiments)
      update-experiments
      ;;
    dashboard)
      dashboard
      ;;
    *)
      echo "Unknown argument ${1}, exiting"
      exit 1
      ;;
  esac
}

SELF="${0}"
COMMAND="${1:-}"

if [ -z "${1:-}" ]; then
  echo "Usage: ${0} info,update-experiments,dashboard"
  exit 1
fi

main "${COMMAND}"
